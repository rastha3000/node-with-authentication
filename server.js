var express = require('express');
global.app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');

global.passport = require('passport');
var config = require('./config/database'); // get db config file

var port = process.env.PORT || 3000;

// get our request parameters
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser());

// log to console
app.use(morgan('dev'));

// Use the passport package in our application
app.use(passport.initialize());

// connect to database
mongoose.connect(config.database);

// ROUTES FOR OUR API
// =============================================================================
var router = require('./routes/router');
app.use('/api', router);

// bear router
var bearRouter = require('./routes/bearRouter');
app.use('/api/bear', bearRouter);

app.listen(port);
console.log('Magic happens on port ' + port);
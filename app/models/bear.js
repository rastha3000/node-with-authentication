// get an instance of mongoose and mongoose.Schema
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

// set up a mongoose model
var BearSchema   = new Schema({
    name: String
});

// pass it using module.exports
module.exports = mongoose.model('Bear', BearSchema);



